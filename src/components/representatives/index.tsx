import React, { useEffect, useState } from 'react';
import { FIRST, LAST, REPRESENTATIVES, SENATORS } from '../../constants';
import { govtOfficialInterface } from '../../interfaces';
import { capitalizeWord, formatAddress, splitName } from '../../services';
import { Table, TableBody, TableCell, TableHead, TableRow, Card, CardContent, Collapse } from '@mui/material';
import styles from './styles.module.css';

interface RepresentativesProps {
	representatives: govtOfficialInterface[];
}

const Representatives: React.FC<RepresentativesProps> = ({ representatives }) => {
	const [individualRep, setIndividualRep] = useState<govtOfficialInterface | null>(null);
	const [expanded, setExpanded] = useState<boolean>(false);
	const govtOfficialType = representatives[0] && representatives[0].district === '' ? SENATORS : REPRESENTATIVES;

	useEffect(() => {
		setExpanded(false);
		setIndividualRep(null);
	}, [representatives]);

	const handleRepresentativeClick = (rep: govtOfficialInterface) =>
		rep === individualRep && !expanded
			? setExpanded(true)
			: rep === individualRep && expanded
			? setExpanded(false)
			: (setExpanded(true), setIndividualRep(rep));

	const getClassNames = (representative: govtOfficialInterface) => {
		const party = representative.party[0];
		if (expanded && individualRep === representative) {
			return party === 'D'
				? `${styles.repRow} ${styles.selectedDemocrat}`
				: party === 'R'
				? `${styles.repRow} ${styles.selectedRepublican}`
				: `${styles.repRow} ${styles.selectedIndependent}`;
		}
		return `${styles.repRow}`;
	};

	const renderRepresentatives = () => (
		<Table className={styles.representativesTable}>
			<TableHead className={styles.tableHeader}>
				<TableRow>
					<TableCell className={styles.headerCell}>Name</TableCell>
					<TableCell className={styles.headerCell}>Party</TableCell>
				</TableRow>
			</TableHead>
			<TableBody className={styles.repTableBody}>
				{representatives.map((rep) => (
					<TableRow key={rep.name} className={getClassNames(rep)} onClick={() => handleRepresentativeClick(rep)}>
						<TableCell className={styles.repCell}>{rep.name}</TableCell>
						<TableCell className={styles.repCell}>{rep.party[0]}</TableCell>
					</TableRow>
				))}
			</TableBody>
		</Table>
	);

	const renderIndividualInfo = (representative: govtOfficialInterface | null) => (
		<div className={styles.individualTable}>
			<Card className={styles.card}>
				<CardContent className={styles.cardContent}>
					<Collapse in={expanded} timeout={1500}>
						{representative !== null ? (
							<Table>
								<TableBody>
									<TableRow className={styles.infoRow}>
										<TableCell className={styles.infoCell}>
											<div className={styles.infoText}>{splitName(representative.name, FIRST)}</div>
										</TableCell>
									</TableRow>
									<TableRow className={styles.infoRow}>
										<TableCell className={styles.infoCell}>
											<div className={styles.infoText}>{splitName(representative.name, LAST)}</div>
										</TableCell>
									</TableRow>
									{govtOfficialType === REPRESENTATIVES ? (
										<TableRow className={styles.infoRow}>
											<TableCell className={styles.infoCell}>
												<div className={styles.infoText}>District: {representative.district}</div>
											</TableCell>
										</TableRow>
									) : null}
									<TableRow className={styles.infoRow}>
										<TableCell className={styles.infoCell}>
											<div className={styles.infoText}>
												Phone #:&nbsp;
												<a href={`tel:+${representative.phone}`} className={styles.link}>
													{representative.phone}
												</a>
											</div>
										</TableCell>
									</TableRow>
									<TableRow className={styles.infoRow}>
										<TableCell className={styles.infoCell}>
											<div className={styles.infoText}>
												Address:&nbsp;
												<a href={formatAddress(representative.office)} className={styles.link}>
													{representative.office}
												</a>
											</div>
										</TableCell>
									</TableRow>
									<TableRow className={styles.infoRow}>
										<TableCell className={styles.infoCell}>
											<div className={styles.infoText}>
												Website:&nbsp;
												<a href={representative.link} className={styles.link}>
													{representative.link}
												</a>
											</div>
										</TableCell>
									</TableRow>
								</TableBody>
							</Table>
						) : (
							<div className={styles.blankSpace} />
						)}
					</Collapse>
				</CardContent>
			</Card>
		</div>
	);

	return (
		<div className={styles.representativesSection}>
			<div className={styles.tableTitles}>
				<span>
					List &nbsp; / &nbsp;
					<span className={styles.officialType}>{capitalizeWord(govtOfficialType)}</span>
				</span>
				<span className={styles.infoTableTitle}>{expanded ? 'Info' : null}</span>
			</div>
			<div className={styles.tablesSection}>
				{representatives.length ? renderRepresentatives() : null}
				{renderIndividualInfo(individualRep)}
			</div>
		</div>
	);
};

export default Representatives;
