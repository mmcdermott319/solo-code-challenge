import React, { useState } from 'react';
import { govtOfficialInterface } from '../../interfaces';
import Representatives from '../representatives';
import SearchArea from '../searchArea';
import styles from './styles.module.css';

const App: React.FC = () => {
	const [representatives, setRepresentatives] = useState<govtOfficialInterface[]>([]);

	const renderHeader = () => (
		<header className={styles.header}>
			<div className={styles.headerText}>Who's My Representative?</div>
		</header>
	);
	return (
		<div className={styles.app}>
			{renderHeader()}
			<SearchArea setRepresentatives={setRepresentatives} />
			<Representatives representatives={representatives} />
		</div>
	);
};

export default App;
