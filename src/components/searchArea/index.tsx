import React, { useCallback, useState } from 'react';
import axios from 'axios';
import { capitalizeWord } from '../../services';
import { govtOfficialInterface } from '../../interfaces';
import { STATESINFOARRAY, STATENAMELONG, STATEABBREVIATIONS } from '../../constants/stateAbbreviations';
import { ERRORMESSAGE, REPRESENTATIVES, SENATORS } from '../../constants';
import styles from './styles.module.css';

interface SearchAreaProps {
	setRepresentatives: (reps: govtOfficialInterface[]) => void;
}

const SearchArea: React.FC<SearchAreaProps> = ({ setRepresentatives }) => {
	const [govtOfficialType, setGovtOfficialType] = useState<string>(REPRESENTATIVES);
	const [searchState, setSearchState] = useState<string>('');
	const [searchBoxText, setSearchBoxText] = useState<string>('');

	const fetchList = useCallback(async () => {
		await axios
			.get(`/${govtOfficialType}/${searchState}`)
			.then((res) => {
				setRepresentatives(res.data.results);
			})
			.catch((err) => {
				console.error(err);
			});
	}, [setRepresentatives, govtOfficialType, searchState]);

	const handleStateSelection = (event: React.FormEvent<HTMLInputElement>) => {
		const userInput = event.currentTarget.value;
		const stateAssocation = STATENAMELONG[userInput];
		setSearchBoxText(userInput);
		return userInput === '' ? setSearchState(userInput) : stateAssocation ? setSearchState(stateAssocation) : null;
	};

	const handleOfficialTypeSelection = (event: React.FormEvent<HTMLInputElement>) => {
		const selection = event.currentTarget.value.toLowerCase();
		return selection !== govtOfficialType ? setGovtOfficialType(selection) : null;
	};

	const handleSearch = () => {
		if (STATEABBREVIATIONS[searchState] !== undefined) {
			fetchList();
		} else {
			alert(ERRORMESSAGE);
		}
	};
	const handleClickReset = () => {
		setSearchState('');
		setSearchBoxText('');
	};
	const getClassNames = (buttonText: string) =>
		buttonText === govtOfficialType ? `${styles.selectedOfficialType}` : `${styles.govtOfficialButton}`;

	return (
		<div className={styles.searchSection}>
			<div className={styles.boxShadow}>
				<input
					type={'button'}
					className={getClassNames(REPRESENTATIVES)}
					value={capitalizeWord(REPRESENTATIVES)}
					onClick={handleOfficialTypeSelection}
				/>
			</div>
			<div className={styles.boxShadow}>
				<input
					type={'button'}
					className={getClassNames(SENATORS)}
					value={capitalizeWord(SENATORS)}
					onClick={handleOfficialTypeSelection}
				/>
			</div>
			<div className={styles.stateBox}>
				<input
					type={'text'}
					className={styles.stateInput}
					list={'states'}
					onChange={handleStateSelection}
					value={searchBoxText}
					placeholder={'Select your state'}
				/>
				<datalist id={'states'}>
					{STATESINFOARRAY.map((state) => (
						<option key={state.abbreviation} label={state.abbreviation} value={state.nonAbbreviation} />
					))}
				</datalist>
				<input type={'button'} className={styles.clearButton} value={'X'} onClick={handleClickReset} />
			</div>
			<input type={'button'} className={styles.searchButton} value={'Search'} onClick={handleSearch} />
		</div>
	);
};

export default SearchArea;
