import { FIRST, LAST } from '../constants';

export const capitalizeWord = (word: string) => word.charAt(0).toUpperCase() + word.slice(1);

export const splitName = (name: string, position: 'first' | 'last') =>
	position === FIRST ? name.split(' ')[0] : position === LAST ? name.split(' ').slice(1).join(' ') : name;

export const formatAddress = (address: string) => `https://www.google.com/maps/search/${address.split(' ').join('+')}`;
