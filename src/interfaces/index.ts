export interface stateAbbreviationsInterface {
	[key: string]: string;
}

export interface govtOfficialInterface {
	[key: string]: string;
	district: string;
	link: string;
	name: string;
	office: string;
	party: string;
	phone: string;
	state: string;
}
