import { handleApiResponse } from '../controllers';
import request from 'request';

export const findRepresentativesByState = (req, res, next) => {
	const url = `http://whoismyrepresentative.com/getall_reps_bystate.php?state=${req.params.state}&output=json`;
	request(url, handleApiResponse(res, next));
};

export const findSenatorsByState = (req, res, next) => {
	const url = `http://whoismyrepresentative.com/getall_sens_bystate.php?state=${req.params.state}&output=json`;
	request(url, handleApiResponse(res, next));
};
