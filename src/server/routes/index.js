import { findRepresentativesByState, findSenatorsByState } from '../models';
import { jsonResponse } from '../controllers';
import cors from 'cors';
import express from 'express';

const app = express();

app.use(cors());

app.get('/representatives/:state', findRepresentativesByState, jsonResponse);

app.get('/senators/:state', findSenatorsByState, jsonResponse);

const server = app.listen(8080, () => {
	const host = server.address().address,
		port = server.address().port;

	console.log('API listening at http://%s:%s', host, port);
});
