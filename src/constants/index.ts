export const REPRESENTATIVES = 'representatives';
export const SENATORS = 'senators';
export const FIRST = 'first';
export const LAST = 'last';
export const ERRORMESSAGE = 'You must select a state first';
